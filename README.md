Website kurmis.com
========================

I setup my website using [github pages][ghp].

The layout is based on layout examples using [Pure CSS][pure] compiled from the [pure-site][] project.

[ghp]: https://pages.github.com/
[pure]: http://purecss.io/
[pure-site]: https://github.com/yahoo/pure-site

Build status
------------

Drone.io [![Build Status](https://drone.io/github.com/oliworx/oliworx.github.com/status.png)](https://drone.io/github.com/oliworx/oliworx.github.com/latest)

Circle CI [![Circle CI](https://circleci.com/gh/oliworx/oliworx.github.com.svg?style=svg)](https://circleci.com/gh/oliworx/oliworx.github.com)

License
-------

This software is free to use under the Yahoo! Inc. BSD license.
See the [LICENSE file][] for license text and copyright information.

[LICENSE file]: https://github.com/yahoo/pure-site/blob/master/LICENSE.md
